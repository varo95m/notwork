package com.carmabs.ema.core.constants

/**
 * <p>
 * Copyright (c) 2019, Babel Sistemas de Información. All rights reserved.
 * </p>
 *
 * @author <a href=“mailto:carlos.mateo@babel.es”>Carlos Mateo</a>
 *
 * Date: 2019-11-08
 */

const val STRING_EMPTY = ""
const val STRING_SPACE = " "
const val STRING_HYPHEN = "-"
const val STRING_SLASH = "/"
const val STRING_DOT = "."