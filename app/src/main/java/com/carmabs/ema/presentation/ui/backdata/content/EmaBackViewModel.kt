package com.carmabs.ema.presentation.ui.backdata.content;

import com.carmabs.ema.android.viewmodel.EmaViewModel
import com.carmabs.ema.presentation.ui.backdata.EmaBackNavigator
import com.carmabs.ema.presentation.ui.backdata.result.EmaBackResultViewModel

/**
 * <p>
 * Copyright (c) 2019, Babel Sistemas de Información. All rights reserved.
 * </p>
 *
 * @author <a href=“mailto:carlos.mateo@babel.es”>Carlos Mateo</a>
 * <p>
 * Date: 2019-11-07
 */

class EmaBackViewModel : EmaViewModel<EmaBackState, EmaBackNavigator.Navigation>() {

    companion object {
        const val RESULT_USER_NUMBER = 1000
    }
    override fun createInitialViewState(): EmaBackState {
        return EmaBackState()
    }

    override fun onStartFirstTime(statePreloaded: Boolean) {

    }

    override fun onResultListenerSetup() {
        addOnResultReceived(EmaBackResultViewModel.RESULT_USER){
            updateViewState {
                val mutableList = listUsers.toMutableList()
                mutableList.add(it.data as EmaBackUserModel)
                setResult(RESULT_USER_NUMBER,mutableList.size)
                copy(listUsers = mutableList,noUserVisibility = mutableList.isEmpty())
            }
        }
    }

    fun onActionAddUser() {
        navigate(EmaBackNavigator.Navigation.Result)
    }
}
