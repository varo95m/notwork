package com.carmabs.ema.presentation.ui.backdata.result

import android.text.Editable
import android.text.TextWatcher
import android.widget.Toast
import com.carmabs.ema.R
import com.carmabs.ema.android.extra.EmaTextWatcher
import com.carmabs.ema.core.state.EmaExtraData
import com.carmabs.ema.presentation.STRING_EMPTY
import com.carmabs.ema.presentation.base.BaseFragment
import com.carmabs.ema.presentation.ui.backdata.EmaBackNavigator
import kotlinx.android.synthetic.main.fragment_back_result.*
import kotlinx.android.synthetic.main.layout_password.*
import kotlinx.android.synthetic.main.layout_user.*
import org.kodein.di.generic.instance

/**
 * <p>
 * Copyright (c) 2019, Babel Sistemas de Información. All rights reserved.
 * </p>
 *
 * @author <a href=“mailto:carlos.mateo@babel.es”>Carlos Mateo</a>
 *
 * Date: 2019-11-07
 */

class EmaBackResultFragment : BaseFragment<EmaBackResultState, EmaBackResultViewModel, EmaBackNavigator.Navigation>() {

    override val inputStateKey: String? = null

    override fun onInitialized(viewModel: EmaBackResultViewModel) {
        setupButtons(viewModel)
        setupEditTexts(viewModel)
    }

    private fun setupEditTexts(viewModel: EmaBackResultViewModel) {

        etBackResultName.addTextChangedListener(EmaTextWatcher {
            viewModel.onActionNameWrite(it)
        })

        etBackResultSurname.addTextChangedListener(EmaTextWatcher {
            viewModel.onActionSurnameWrite(it)
        })
    }


    private fun setupButtons(viewModel: EmaBackResultViewModel) {

        bBackResultAccept.setOnClickListener {
            val name = etBackResultName.text.toString()
            val surname = etBackResultSurname.text.toString()
            viewModel.onActionAddUser(
                    name = name,
                    surname = surname
            )
        }
    }

    override fun getFragmentLayout(): Int = R.layout.fragment_back_result

    override
    val viewModelSeed: EmaBackResultViewModel by instance()

    override
    val navigator: EmaBackNavigator by instance()

    override fun onNormal(data: EmaBackResultState) {
        etBackResultName.setText(data.name)
        etBackResultSurname.setText(data.surname)
    }

    override fun onLoading(data: EmaExtraData) {
    }

    override fun onSingle(data: EmaExtraData) {
        Toast.makeText(requireContext(), data.extraData as String, Toast.LENGTH_SHORT).show()
    }

    override fun onError(error: Throwable) {

    }
}