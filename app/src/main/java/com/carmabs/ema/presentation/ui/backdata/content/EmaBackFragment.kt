package com.carmabs.ema.presentation.ui.backdata.content;

import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.carmabs.ema.R
import com.carmabs.ema.core.state.EmaExtraData
import com.carmabs.ema.presentation.base.BaseFragment
import com.carmabs.ema.presentation.ui.backdata.EmaBackNavigator
import kotlinx.android.synthetic.main.fragment_back.*
import org.kodein.di.generic.instance

/**
 * <p>
 * Copyright (c) 2019, Babel Sistemas de Información. All rights reserved.
 * </p>
 *
 * @author <a href=“mailto:carlos.mateo@babel.es”>Carlos Mateo</a>
 * <p>
 * Date: 2019-11-07
 */

class EmaBackFragment : BaseFragment<EmaBackState, EmaBackViewModel, EmaBackNavigator.Navigation>() {

    override val inputStateKey: String? = null

    private val adapter : EmaBackAdapter by lazy { EmaBackAdapter() }
    override fun onInitialized(viewModel: EmaBackViewModel) {
        setupRecycler()
        setupButton(viewModel)
    }

    private fun setupButton(viewModel: EmaBackViewModel) {
        bBack.setOnClickListener {
            viewModel.onActionAddUser()
        }
    }

    private fun setupRecycler() {
        rvBack.layoutManager = LinearLayoutManager(requireContext(),RecyclerView.VERTICAL,false)
        rvBack.adapter = adapter
    }

    override val fragmentViewModelScope: Boolean = true

    override fun getFragmentLayout(): Int  = R.layout.fragment_back

    override val viewModelSeed: EmaBackViewModel by instance()

    override val navigator: EmaBackNavigator by instance()

    override fun onNormal(data: EmaBackState) {
        adapter.updateList(data.listUsers)
        tvBackNoUsers.visibility = checkVisibility(data.noUserVisibility)
        rvBack.visibility = checkVisibility(!data.noUserVisibility,gone = false)
    }

    override fun onLoading(data: EmaExtraData) {

    }

    override fun onSingle(data: EmaExtraData) {

    }

    override fun onError(error: Throwable) {

    }

}
