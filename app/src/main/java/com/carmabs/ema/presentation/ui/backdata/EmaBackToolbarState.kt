package com.carmabs.ema.presentation.ui.backdata

import com.carmabs.ema.core.state.EmaBaseState

/**
 * <p>
 * Copyright (c) 2019, Babel Sistemas de Información. All rights reserved.
 * </p>
 *
 * @author <a href=“mailto:carlos.mateo@babel.es”>Carlos Mateo</a>
 *
 * Date: 2019-11-07
 */

data class EmaBackToolbarState(
        val default: Boolean = false
) : EmaBaseState