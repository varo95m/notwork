package com.carmabs.ema.presentation.ui.backdata

import com.carmabs.ema.android.viewmodel.EmaViewModel

/**
 * <p>
 * Copyright (c) 2019, Babel Sistemas de Información. All rights reserved.
 * </p>
 *
 * @author <a href=“mailto:carlos.mateo@babel.es”>Carlos Mateo</a>
 *
 * Date: 2019-11-07
 */

class EmaBackToolbarViewModel : EmaViewModel<EmaBackToolbarState, EmaBackNavigator.Navigation>() {

    companion object{
        const val RESULT_USERS_ADDED = 0
    }
    override fun createInitialViewState(): EmaBackToolbarState {
        return EmaBackToolbarState()
    }

    override fun onStartFirstTime(statePreloaded: Boolean) {
    }
}