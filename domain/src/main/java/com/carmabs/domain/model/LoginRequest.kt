package com.carmabs.domain.model

/**
 * TODO: Add a class header comment.
 *
*
 *
 * @author <a href=“mailto:apps.carmabs@gmail.com”>Carlos Mateo</a>
 */

data class LoginRequest(val name: String = "", val password: String = "")