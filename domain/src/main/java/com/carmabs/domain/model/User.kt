package com.carmabs.domain.model

import java.io.Serializable

/**
 * TODO: Add a class header comment.
 *
*
 *
 * @author <a href=“mailto:apps.carmabs@gmail.com”>Carlos Mateo</a>
 */

data class User(val name: String = "", val surname: String = "") : Serializable