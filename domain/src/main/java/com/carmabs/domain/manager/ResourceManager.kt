package com.carmabs.domain.manager

/**
 * <p>
 * Copyright (c) 2019, Babel Sistemas de Información. All rights reserved.
 * </p>
 *
 * @author <a href=“mailto:carlos.mateo@babel.es”>Carlos Mateo</a>
 *
 * Date: 2019-11-08
 */

interface ResourceManager {

    fun getResultErrorFillName():String

    fun getResultErrorFillSurname():String
}